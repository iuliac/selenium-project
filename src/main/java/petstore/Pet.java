package petstore;

public class Pet {

    private long id;
    private String name;
    private String [] photoUrls;
    private String [] tags;
    private String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String[] getPhotoUrls() {
        return photoUrls;
    }

    public String[] getTags() {
        return tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setPhotoUrls(String[] photoUrls) {
        this.photoUrls = photoUrls;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }
    public Pet(){}
    public Pet(String name) {
        this.name = name;
    }

    public Pet(long id, String name) {
        this.id = id;
        this.name=name;
    }
}
