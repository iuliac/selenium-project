package shop.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import shop.model.User;

public class Register {

    @FindBy(id = "reg_email")
    private WebElement email;

    @FindBy(id = "reg_password")
    private WebElement password;

    @FindBy(id = "registration_field_1")
    private WebElement firstName;

    @FindBy(id = "registration_field_2")
    private WebElement lastname;

    @FindBy(id = "registration_field_3")
    private WebElement phoneNumber;

    @FindBy(name="register")
    private WebElement registerBtn;


    public void register(User user) {
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getPassword());
        firstName.sendKeys(user.getFirstName());
        lastname.sendKeys(user.getLastName());
        phoneNumber.sendKeys(user.getPhoneNumber());
        registerBtn.click();
    }

}
