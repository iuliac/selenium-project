package shop.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import shop.model.User;

public class LoginPageFactory {

    @FindBy (xpath = "//div[@id='customer_login']/div[contains(@class,'u-column1']/h2")
    private WebElement loginTitle;

    @FindBy(id="username")
    private WebElement usernameEdit;

    @FindBy(id="password")
    private WebElement passwordEdit;

    @FindBy(xpath = "//input[@name='login']")
    private WebElement loginBtn;

    public void fillUsername(String username){
        usernameEdit.clear();
        usernameEdit.sendKeys(username);
    }
    public void fillPassword(String password){
        passwordEdit.clear();
        passwordEdit.sendKeys(password);
    }

    public void clickLogin(){
        loginBtn.click();
    }

    public void loginWithUsernameAndPassword(String username, String password){
        fillUsername(username);
        fillPassword(password);
        clickLogin();
    }

    public void loginWithEmailUser(User user) {
        fillUsername(user.getEmail());
        fillPassword(user.getPassword());
        clickLogin();
    }
}
