package shop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class HomePageCollection {

    private WebElement container;


    @FindBy(xpath = ".//h3[@class='page-title']/a")
    private WebElement title;

    @FindBy(xpath = ".//div[@class='sorting-form-wrapper']/a")
    private WebElement viewAllBtn;

    private static final String PRODUCT_LIST_XPATH = "(.//div[@class='product-list-wrap'])[<<index>>]";

    public HomePageCollection(WebElement container) {
        PageFactory.initElements(new DefaultElementLocatorFactory(container), this);
        this.container = container;
    }

    public String getCollectionTitle() {
        return title.getText();
    }

    public CollectionProductsList getcollectionProductList(int index) {
        WebElement productList = container.findElement(By.xpath(PRODUCT_LIST_XPATH.replace("<<index>>", String.valueOf(index))));
        return new CollectionProductsList(productList);

    }
}
