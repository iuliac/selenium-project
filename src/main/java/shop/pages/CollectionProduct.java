package shop.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class CollectionProduct {
    private WebElement container;
    private WebDriver driver;

    @FindBy(tagName = "figure")
    private WebElement image;

    @FindBy(className = "product-list-title")
    private WebElement title;

    @FindBy(className = "price")
    private WebElement price;

    @FindBy(className = "cart-wishlisy-btn")
    private WebElement addToCartBtn;

    public CollectionProduct(WebElement container) {
        PageFactory.initElements(new DefaultElementLocatorFactory(container), this);

        this.container = container;
    }

    public String getTitle(){
        return title.getText();
    }

    public String getPrice(){
        return price.getText();
    }
    public void addToCart(){
        addToCartBtn.click();
    }

}
