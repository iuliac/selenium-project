package shop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageContent {

    private WebDriver driver;

    @FindBy(xpath = "//h3[@class='slider-title']/a[contains(text(),'Men Collection')]")
    private WebElement menCollectionBtn;
    @FindBy(xpath = "//h3[@class='slider-title']/a[contains(text(),'Women Collection')]")
    private WebElement womenCollectionBtn;

    private static final String HOME_PAGE_COLLECTION_XPATH ="//section[@id='estore_woocommerce_product_grid-<<collectionNumber>>']";

    public HomePageContent(WebDriver driver){
        PageFactory.initElements(driver,this);
       this.driver= driver;

    }

    public void clickOnMenCollectionBtn(){
        menCollectionBtn.click();
    }
    public void clickOnWomenCollectionBtn(){
        womenCollectionBtn.click();
    }

    public HomePageCollection getHomePageCollectionByIndex(int index){
       WebElement homePageCollectionContainer=driver.findElement(By.xpath(HOME_PAGE_COLLECTION_XPATH.replace("<<collectionNumber>>", String.valueOf(index))));
        return  new HomePageCollection(homePageCollectionContainer);
    }
}
