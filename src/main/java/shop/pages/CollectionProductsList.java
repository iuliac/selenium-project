package shop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import java.util.List;

public class CollectionProductsList {

    private WebElement container;
    private static final String PRODUCT_XPATH= "./div";

    private static final String PRODUCT_WITH_NAME_XPATH = PRODUCT_XPATH+ "[.//a[text()='<<productName>>']]";

    public CollectionProductsList(WebElement container) {
      //  PageFactory.initElements(new DefaultElementLocatorFactory(container), this);
        this.container = container;
    }

    public int getNumberOfProducts(){
        List<WebElement> products= container.findElements(By.xpath(PRODUCT_XPATH));
        return  products.size();
    }

    public CollectionProduct getProductFromList(String productName){
        WebElement product= container.findElement(By.xpath(PRODUCT_WITH_NAME_XPATH.replace("<<productName>>", productName)));

        return new CollectionProduct(product);
    }
}
