package shop.pages;

import com.sun.org.apache.regexp.internal.RE;
import org.apache.commons.lang3.ObjectUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

    private static final By USERNAME_FIELD = By.id("username");//locator, nu webelement
    private static final By PASSWORD_FIELS = By.id("password");
    private static final By LOGIN_BUTTON = By.xpath("//input[@name='login']");
    private static final By REMEMBER_CHECKBOX = By.id("rememberme");
    private static final By LOST_PASSWORD = By.cssSelector(".lost_password a");
    private static final String LOGIN_ERROR= "//ul[@class='woocommerce-error']/li[contains(text(),'<<errorType>>')]";

   /* private static final By ERROR_USERNAME_EMPTY = By.xpath("//ul[@class='woocommerce-error']/li[contains(text(),' Username is required.')]");
    private static final By ERROR_PASSWORD_EMPTY = By.xpath("//ul[@class='woocommerce-error']/li[contains(text(),': The password field is empty.')]");
    private static final By ERROR_PASSWORD_INVALID = By.xpath("//ul[@class='woocommerce-error']/li[contains(text(),': The password you entered for the username')]");
    private static final By ERROR_USERNAME_INVALID = By.xpath("//ul[@class='woocommerce-error']/li[contains(text(),': Invalid username. ')]");*/


    private static final By ERROR_EMAIL_INVALID = By.xpath("//ul[@class='woocommerce-error']/li[contains(text(),'A user could not be found with this email address.')]");

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillUsername(String username) {
        WebElement usernameElement = driver.findElement(USERNAME_FIELD);
        usernameElement.clear();
        usernameElement.sendKeys(username);

    }

    public void fillPassword(String password) {
        driver.findElement(PASSWORD_FIELS).sendKeys(password);
    }

    public void clickOnLogin() {
        driver.findElement(LOGIN_BUTTON).click();
    }

   /* public WebElement getErrorUsernameEmpty() {
        return driver.findElement(ERROR_USERNAME_EMPTY);
    }

    public WebElement getErrorPasswordEmpty() {
        return driver.findElement(ERROR_PASSWORD_EMPTY);
    }

    public WebElement getErrorPasswordInvalid() {
        return driver.findElement(ERROR_PASSWORD_INVALID);
    }

    public WebElement getErrorUsernameInvalid() {
        return driver.findElement(ERROR_USERNAME_INVALID);
    }

    public WebElement getErrorEmailInvalid() {
        return driver.findElement(ERROR_EMAIL_INVALID);
    }
*/
    public WebElement checkRememberMe() {
        WebElement rememberme = driver.findElement(REMEMBER_CHECKBOX);
        rememberme.click();
        return rememberme;
    }

    public void clickLostPassword() {
        driver.findElement(LOST_PASSWORD).click();
    }

    public WebElement getErrorMesage(String type){
        WebElement error;
        switch (type) {
            case"username_empty":
                error=driver.findElement(By.xpath(LOGIN_ERROR.replace("<<errorType>>", " Username is required.")));
                return  error;
            case"password_empty":
                error= driver.findElement(By.xpath(LOGIN_ERROR.replace("<<errorType>>", ": The password field is empty.")));
                return error;
            case "username_invalid":
                error= driver.findElement(By.xpath(LOGIN_ERROR.replace("<<errorType>>",": Invalid username. ")));
                return error;
            case"password_invalid":
                error=driver.findElement(By.xpath(LOGIN_ERROR.replace("<<errorType>>", ": The password you entered for the username")));
                return error;
            case"email_invalid":
                error= driver.findElement(By.xpath(LOGIN_ERROR.replace("<<errorType>>"," A user could not be found with this email address." )));
                return error;
        }
        return null;
    }
}
