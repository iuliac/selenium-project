package shop.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Home {

    @FindBy(id = "top_slider_section")
    private WebElement slider;

    public boolean isSliderDisplayed(){
        return slider.isDisplayed();
    }
}
