package shop.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import shop.model.User;

public class Login {

   @FindBy(id="username")
   private WebElement username;

   @FindBy(id="password")
    private WebElement password;

   @FindBy(xpath = "//input[@name='login']")
   private WebElement loginButton;


   public void signIn(User user ){
       username.sendKeys(user.getEmail());
       password.sendKeys(user.getPassword());
       loginButton.click();
   }
}
