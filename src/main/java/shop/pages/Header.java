package shop.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header {

    private WebDriver driver;

    @FindBy(id="site-title")
    private WebElement title;

    @FindBy(xpath = "//a[@class='single_image_with_link']//img")
    private WebElement mainImage;

    @FindBy(xpath = "//a[@class='wcmenucart-contents']")
    private WebElement cartBtn;

    @FindBy(xpath = "//a[contains(text(),'Blog')]")
    private WebElement ribbonBlogButton;

    @FindBy(xpath = "//a[contains(text(),'Home')]")
    private WebElement ribbonHomeButton;

    @FindBy(xpath = "//a[contains(text(),'Connect')]")
    private WebElement ribbonConnectButton;

    public Header(WebDriver driver)
    {
        this.driver=driver;
    }

    public boolean isTitleEqualTo(String expected){
        return title.getText().equals(expected);
    }

    public boolean isMainImageDisplayed(){
        return mainImage.isDisplayed();
    }

    public boolean isCartButtonDisplayed(){
        return  cartBtn.isDisplayed();
    }

    public Blog goToBlog(){
        ribbonBlogButton.click();
        Blog blog = PageFactory.initElements(driver, Blog.class);

        return blog;
    }

    public Home goToHome(){
        ribbonHomeButton.click();
        return PageFactory.initElements(driver,Home.class);
    }
    public Register goToRegister(){
        ribbonConnectButton.click();
        return PageFactory.initElements(driver, Register.class);
    }
    public Login goToLogin(){
        ribbonConnectButton.click();
        return PageFactory.initElements(driver, Login.class);
    }

}
