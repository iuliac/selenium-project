package whitelotus;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static utils.BaseTestClass.highlightElement;
import static utils.DriversUtils.setWebdriverProperty;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class CalendarTests {
    private static ChromeDriver driver;
    private final WebDriverWait wait = new WebDriverWait(driver, 10);

    @BeforeClass
    public static void setup() {
        setWebdriverProperty("chrome");
        driver = new ChromeDriver();
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/");
        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('user','conut.iulia@gmail.com')");
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/#/calendar");
        driver.manage().window().maximize();
    }

    @Test
    public void calendarTest_1() {

        WebElement monthBtn = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".react-calendar__navigation__label")));
        highlightElement(driver, monthBtn);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
        String currentMonth = sdf.format(calendar.getTime());
        assertEquals(currentMonth, monthBtn.getText());

        WebElement nextMonthNavBtn = wait.until(ExpectedConditions
                .elementToBeClickable(By.xpath("//button[@class='react-calendar__navigation__arrow react-calendar__navigation__next-button']")));
        highlightElement(driver, nextMonthNavBtn);
        nextMonthNavBtn.click();
        highlightElement(driver, monthBtn);
        calendar.add(Calendar.MONTH, 1);
        String nextMonth = sdf.format(calendar.getTime());
        assertEquals(nextMonth, monthBtn.getText());

    }

    @Test
    public void calendarTest_2() {
        WebElement monthBtn = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".react-calendar__navigation__label")));
        highlightElement(driver, monthBtn);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
        String month = sdf.format(calendar.getTime());
        assertEquals(month, monthBtn.getText());

        WebElement nextYearNavBtn = wait.until(ExpectedConditions
                .elementToBeClickable(By.xpath("//button[@class='react-calendar__navigation__arrow react-calendar__navigation__next2-button']")));
        highlightElement(driver, nextYearNavBtn);
        nextYearNavBtn.click();
        highlightElement(driver, monthBtn);
        calendar.add(Calendar.YEAR, 1);
        String nextYearMonth = sdf.format(calendar.getTime());
        assertEquals(nextYearMonth, monthBtn.getText());

        WebElement prevYearNavBtn = wait.until(ExpectedConditions
                .elementToBeClickable(By.xpath("//button[@class='react-calendar__navigation__arrow react-calendar__navigation__prev2-button']")));
        highlightElement(driver, prevYearNavBtn);
        prevYearNavBtn.click();
        highlightElement(driver, monthBtn);
        assertEquals(month, monthBtn.getText());

    }

    @Test
    public void calendarTest_3() {
        List<WebElement> daysSummary = wait.until(ExpectedConditions
                .visibilityOfAllElementsLocatedBy(By.xpath("//div[@class='calendar-day-row row']")));
        Calendar calendar = Calendar.getInstance();
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        assertEquals(daysInMonth, daysSummary.size());
    }

    @Test
    public void calendarTest_4() {
        WebElement dayBtn = wait.until(ExpectedConditions
                .elementToBeClickable(By.xpath("//abbr[@aria-label='November 3, 2019']")));
        highlightElement(driver, dayBtn);
        dayBtn.click();

        WebElement cardHeader = wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//div[@class='text-center p-3 card border-success']/div[@class='card-header']")));
        highlightElement(driver, cardHeader);
        String cardTitle = cardHeader.getText();
        System.out.println(cardTitle);

        List<WebElement> daysSummary = wait.until(ExpectedConditions
                .visibilityOfAllElementsLocatedBy(By.xpath("//div[@class='calendar-day-row row']")));

        String dayText = daysSummary.get(2).getText();
        System.out.println(dayText);
        assertTrue(dayText.contains(cardTitle));
    }

    @AfterClass
    public static void teardown() {
        driver.close();
    }

}

