package data_driven;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(DataProviderRunner.class)
public class DataDrivenTests {
    @DataProvider
    public static Object[][] powers() {
        return new Object[][]{
                {0, 1},
                {1, 2},
                {2, 4},
                {3, 8},
                {4, 16}
        };
    }

    @UseDataProvider("powers")
    @Test
    public void testPowersOf2(int a, int b) {
        System.out.println(a + ", " + b);
        assertEquals(b, (int) Math.pow(2, a));
    }

    @DataProvider
    public static Object[][] CSVDataProvider() throws IOException {
        List<Object[]> outputList = new ArrayList<>();
        List<String> records = readAllLinesFromResourcesFile("users_db.csv");

        for (String record : records) {
            Object[] params = record.split(",");
            outputList.add(params);
        }

        Object[][] result = new Object[records.size()][];
        outputList.toArray(result);
        return result;
    }


    @UseDataProvider("CSVDataProvider")
    @Test
    public void testUsingCSVDataProvider(String email, String password, String name, String id) {
        System.out.println(name + " (id: " + id + ") has email " + email + " and pass >" + password + "<");
    }

    @DataProvider
    public static Object[][] ExcelDataProvider() throws IOException {
        List<Object[]> outputList = new ArrayList<>();
        List<List<Object>> records = readAllCellsFromResourcesExcelFile("users_db.xlsx");

        // Because we received lists of objects, we have to convert them to arrays of objects
        for (List<Object> record : records) {
            outputList.add(record.toArray());
        }

        Object[][] result = new Object[records.size()][];
        outputList.toArray(result);
        return result;
    }


    @UseDataProvider("ExcelDataProvider")
    @Test
    public void testUsingExcelDataProvider(String email, String password, double id, String name) {
        System.out.println(name + " (id: " + id + ") has email " + email + " and pass >" + password + "<");
    }

    /**
     * Read all cells into a matrix of objects.
     * Fill in missing cells with empty strings
     *
     * @param fileName file path relative to the resources folder
     */
    public static List<List<Object>> readAllCellsFromResourcesExcelFile(String fileName) throws IOException {
        File file = getFileFromResources(fileName);
        FileInputStream excelFile = new FileInputStream(file);
        Workbook workbook = new XSSFWorkbook(excelFile);
        Sheet datatypeSheet = workbook.getSheetAt(0);
        Iterator<Row> rowsIterator = datatypeSheet.iterator();

        List<List<Object>> result = new ArrayList<>();

        int columnCount = -1;
        while (rowsIterator.hasNext()) {
            Row currentRow = rowsIterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();

            List<Object> rowValues = new ArrayList<>();
            // Loop through all the cells
            int lastCellIndex = -1;
            while (cellIterator.hasNext()) {
                Cell currentCell = cellIterator.next();
                lastCellIndex++;
                if (columnCount < currentCell.getColumnIndex()) columnCount = currentCell.getColumnIndex();

                // If a row contains empty cells, the iterator of cells will skip them
                // So the row:
                // "alpha" | | "beta"
                // will be returned as ["alpha", "beta"]
                // So if we have such cases, we add empty strings out of the box
                // WARNING: This assumes the cell type is String, and might cause crashes if we expect numeric values
                //
                while (lastCellIndex < currentCell.getColumnIndex()) {
                    rowValues.add("");
                    lastCellIndex++;
                }

                // Excel has many types of cell values
                // If you use boolean or other types, consider using a switch
                if (currentCell.getCellType() == CellType.STRING)
                    rowValues.add(currentCell.getColumnIndex(), currentCell.getStringCellValue());
                else if (currentCell.getCellType() == CellType.NUMERIC)
                    rowValues.add(currentCell.getColumnIndex(), currentCell.getNumericCellValue());
            }

            // Hack to fill in blank values for empty cells at the end
            // Consider this example
            // "alpha" | "beta" | "gamma"
            // "theta" | "zet" |
            // This will produce [["alpha", "beta", "gamma"], ["theta", "zet"]]
            // And the theta-zet row will crash our data provider because it is too short
            while(rowValues.size() < columnCount+1)
                rowValues.add("");

            result.add(rowValues);
        }
        return result;
    }

    /**
     * Can read a given file from the resources folder
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        File file = getFileFromResources(fileName);

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

    public static File getFileFromResources(String fileName) {
        ClassLoader classLoader = DataDrivenTests.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }
}
