package shop;

import shop.pages.LoginPage;
import utils.BaseTestClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class LoginPageTests extends BaseTestClass {

    String url= "https://testare-automata.practica.tech/shop/?page_id=560";
    @Test
    public void testLoginElementsDisplayed() {
        driver.navigate().to(url);
        WebElement username = driver.findElement(By.id("username"));
        WebElement password = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.xpath("//input[@name='login']"));
        WebElement rememberCheckbox = driver.findElement(By.id("rememberme"));
        WebElement lostPassword = driver.findElement(By.cssSelector(".lost_password a"));

        assertTrue(username.isDisplayed());
        assertTrue(password.isDisplayed());
        assertTrue(loginButton.isDisplayed());
        assertTrue(rememberCheckbox.isDisplayed());
        assertTrue(lostPassword.isDisplayed());
    }

    @Test
    public void testValidLoginEmailAndPassword() {
        driver.navigate().to(url);
        String username = "for_youlia@yahoo.com";
        String password = "Password_Test@";

        LoginPage loginPage = new LoginPage(driver);
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        WebElement myAccountLabel = driver.findElement(By.cssSelector(".entry-title"));
        assertEquals("MY ACCOUNT", myAccountLabel.getText());
    }

    @Test
    public void testValidLoginUsernameAndPassword() {
        driver.navigate().to(url);
        String username = "for_youlia";
        String password = "Password_Test@";

        LoginPage loginPage = new LoginPage(driver);
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        WebElement myAccountLabel = driver.findElement(By.cssSelector(".entry-title"));
        assertEquals("MY ACCOUNT", myAccountLabel.getText());
    }

    @Test
    public void testEmptyEmailAndPasswordLogin() {
        driver.navigate().to(url);
        LoginPage loginPage = new LoginPage(driver);
        String username = "";
        String password = "";
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        assertTrue(loginPage.getErrorMesage("username_empty").getText().contains("Username is required."));
    }

    @Test
    public void testValidEmailAndEmptyPasswordLogin() {
        driver.navigate().to(url);
        LoginPage loginPage = new LoginPage(driver);
        String username = "for_youlia@yahoo.com";
        String password = "";
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        assertTrue(loginPage.getErrorMesage("password_empty").getText().contains("The password field is empty."));
    }

    @Test
    public void testValidUsernameAndEmptyPasswordLogin() {
        driver.navigate().to(url);
        LoginPage loginPage = new LoginPage(driver);
        String username = "for_youlia";
        String password = "";
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        assertTrue(loginPage.getErrorMesage("password_empty").getText().contains("The password field is empty."));
    }

    @Test
    public void testValidEmailAndInvalidPasswordLogin() {
        driver.navigate().to(url);
        LoginPage loginPage = new LoginPage(driver);
        String username = "for_youlia@yahoo.com";
        String password = "Password_Test";
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        assertTrue(loginPage.getErrorMesage("password_invalid").getText()
                .contains("The password you entered for the username for_youlia@yahoo.com is incorrect."));
    }

    @Test
    public void testValidUsernameAndInvalidPasswordLogin() {
        driver.navigate().to(url);
        LoginPage loginPage = new LoginPage(driver);
        String username = "for_youlia";
        String password = "Password_Test";
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        assertTrue(loginPage.getErrorMesage("password_invalid").getText()
                .contains("The password you entered for the username for_youlia is incorrect."));
    }

    @Test
    public void testInvalidEmailAndValidPasswordLogin() {
        driver.navigate().to(url);
        LoginPage loginPage = new LoginPage(driver);
        String username = "for_youlia@yahoo.comm";
        String password = "Password_Test@";
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        assertTrue(loginPage.getErrorMesage("email_invalid").getText()
                .contains("A user could not be found with this email address."));
    }

    @Test
    public void testInvalidUsernameAndValidPasswordLogin() {
        driver.navigate().to(url);
        LoginPage loginPage = new LoginPage(driver);
        String username = "for_youliaa";
        String password = "Password_Test@";
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLogin();

        assertTrue(loginPage.getErrorMesage("username_invalid").getText().contains("Invalid username. "));
    }

    @Test
    public void testRememberCheckboxIsEnabled() throws InterruptedException {
        driver.navigate().to(url);
        LoginPage loginPage= new LoginPage(driver);

        assertTrue(loginPage.checkRememberMe().isSelected());

        Thread.sleep(2000);
        assertFalse(loginPage.checkRememberMe().isSelected());
    }

    @Test
    public void testLostPasswordLink(){
        driver.navigate().to(url);
        LoginPage loginPage= new LoginPage(driver);
        loginPage.clickLostPassword();

        WebElement lostPassMessage= driver.findElement(By.xpath("//p[contains(text(),'Lost your password?')]"));//"//form[@class='woocommerce-ResetPassword lost_reset_password']/p[1]"

        assertTrue(lostPassMessage.getText().contains("Lost your password?"));

    }

}
