package shop;

import shop.pages.*;
import utils.BaseTestClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import shop.model.User;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class HomePageTests extends BaseTestClass {

    /*@BeforeClass
    public static void setup() {
        setWebdriverProperty("chrome");
        driver = new ChromeDriver();
        driver.get("https://practica.wantsome.ro/shop/");

    }*/

    @Test
    public void checkHomePageElements() {
        Header header = PageFactory.initElements(driver, Header.class);
        assertTrue(header.isTitleEqualTo("Wantsome Shop"));
        assertTrue(header.isMainImageDisplayed());
        assertTrue(header.isCartButtonDisplayed());

    }

    @Test
    public void checkBlogButtonFunctionality() {
        Header header = PageFactory.initElements(driver, Header.class);
        Blog blog = header.goToBlog();
        assertTrue(blog.isTitleEqualTo("BLOG"));
    }

    @Test
    public void checkHomeSliderisDisplayed() {
        driver.navigate().to("https://practica.wantsome.ro/shop/?page_id=529");
        Header header = PageFactory.initElements(driver, Header.class);
        Home home = header.goToHome();
        assertTrue(home.isSliderDisplayed());
    }

    @Test
    public void checkLoginPage() {
        Header header = PageFactory.initElements(driver, Header.class);
        Login login = header.goToLogin();
        User user = new User.Builder()
                .withEmail("for_youlia")
                .withPassword("Password_Test@")
                .build();
        login.signIn(user);

        WebElement myAccountLabel = driver.findElement(By.cssSelector(".entry-title"));
        highlightElement(driver, myAccountLabel);
        assertEquals("MY ACCOUNT", myAccountLabel.getText());
    }

    @Test
    public void checkRegisterPage() throws InterruptedException {
        Header header = PageFactory.initElements(driver, Header.class);
        Register reg = header.goToRegister();
        User user = new User.Builder()
                .withEmail("test@user")
                .withPassword("pass_Word123")
                .withFirstName("User")
                .withLastName("Test")
                .withPhoneNumber("074444444")
                .build();

        reg.register(user);
        WebElement errorlabel = driver.findElement(By.cssSelector(".woocommerce-error"));
        scrollToElement(driver, errorlabel);
        highlightElement(driver, errorlabel);
        assertEquals("Error: Please provide a valid email address.", errorlabel.getText());

    }
}
