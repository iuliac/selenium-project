package shop;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import static utils.BaseTestClass.highlightElement;
import static utils.DriversUtils.setWebdriverProperty;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class SearchPageTests {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebdriverProperty("chrome");
        driver = new ChromeDriver();
        driver.get("https://practica.wantsome.ro/shop/?page_id=11");
        driver.manage().window().maximize();
    }

    @Test
    public void test1() {

        WebElement searchButton = driver.findElement(By.xpath("//div[@class='search-wrapper search-user-block']"));
        highlightElement(driver, searchButton);
        searchButton.click();
        WebElement searchBox = driver.findElement(By.xpath("//div[@class='header-search-box active']//input[@placeholder='Search …']"));
        highlightElement(driver, searchBox);
        searchBox.sendKeys("men collection");
        assertEquals("men collection", searchBox.getAttribute("value"));
    }

    @Test
    public void test2() {
        WebElement searchButton = driver.findElement(By.xpath("//div[@class='search-wrapper search-user-block']"));
        searchButton.click();
        WebElement searchBox = driver.findElement(By.xpath("//div[@class='header-search-box active']//input[@placeholder='Search …']"));
        searchBox.sendKeys(("men collection"), (Keys.ENTER));
        assertTrue(driver
                .findElement(By.xpath("//p[contains(text(), 'Sorry, but nothing matched your search terms.')]"))
                .getText().contains("Sorry, but nothing matched your search terms."));

    }

    @Test
    public void test3() throws InterruptedException {
        driver.navigate().to("https://practica.wantsome.ro/shop/?product_cat=men-collection");
        WebElement orderByDropdown = driver.findElement(By.cssSelector(".orderby"));
        Select select = new Select(orderByDropdown);
        select.selectByValue("popularity");
        // assertTrue(orderByDropdown.getText().contains("Sort by popularity"));
    }

    @AfterClass
    public static void teardown() {
         driver.close();
    }

}


