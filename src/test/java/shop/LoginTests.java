package shop;

import utils.BaseTestClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class LoginTests extends BaseTestClass {

    String url= "https://testare-automata.practica.tech/shop/?page_id=560";

    @Test
    public void loginTest_1() {

        driver.navigate().to(url);
        WebElement loginLabel = driver.findElement(By.xpath("//h2[contains(text(),'Login')]"));
        highlightElement(driver, loginLabel);
        assertEquals("Login", loginLabel.getText());
    }

    @Test
    public void loginTest_2() {

        driver.navigate().to(url);
        WebElement usernameTextBox = driver.findElement(By.id("username"));
        highlightElement(driver, usernameTextBox);
        usernameTextBox.sendKeys("test user");
        assertEquals("test user", usernameTextBox.getAttribute("value"));
    }

    @Test
    public void loginTest_3() {

        driver.navigate().to(url);
        WebElement passwordTextBox = driver.findElement(By.cssSelector("#password"));
        highlightElement(driver, passwordTextBox);
        passwordTextBox.sendKeys("password01");
        assertEquals("password01", passwordTextBox.getAttribute("value"));
    }

    @Test
    public void loginTest_4() {

        driver.navigate().to(url);
        WebElement loginButton = driver.findElement(By.xpath("//input[@name='login']"));
        highlightElement(driver, loginButton);
        loginButton.click();

        WebElement errorLabel = driver.findElement(By.xpath("//strong[contains(text(),'Error:')]"));
        highlightElement(driver, errorLabel);
        assertEquals("Error:", errorLabel.getText());
    }

    @Test
    public void loginTest_5() {

        driver.navigate().to(url);
        WebElement usernameTextBox = driver.findElement(By.id("username"));
        highlightElement(driver, usernameTextBox);
        usernameTextBox.sendKeys("test user");
        usernameTextBox.sendKeys(Keys.ENTER);

        WebElement errorLabel = driver.findElement(By.xpath("//li[contains(text(),': The password field is empty.')]"));
        highlightElement(driver, errorLabel);
        assertEquals("ERROR: The password field is empty.", errorLabel.getText());
    }

    @Test
    public void loginTest_6() {

        driver.navigate().to(url);
        WebElement passwordTextBox = driver.findElement(By.cssSelector("#password"));
        highlightElement(driver, passwordTextBox);
        passwordTextBox.sendKeys("password01");
        passwordTextBox.sendKeys(Keys.ENTER);

        WebElement errorLabel = driver.findElement(By.xpath("//li[contains(text(),'Username is required.')]"));
        highlightElement(driver, errorLabel);
        assertEquals("Error: Username is required.", errorLabel.getText());
    }

    @Test
    public void loginTest_7() {

        driver.navigate().to(url);
        WebElement usernameTextBox = driver.findElement(By.id("username"));
        highlightElement(driver, usernameTextBox);
        usernameTextBox.sendKeys("test user");

        WebElement passwordTextBox = driver.findElement(By.cssSelector("#password"));
        highlightElement(driver, passwordTextBox);
        passwordTextBox.sendKeys("password01");

        WebElement loginButton = driver.findElement(By.xpath("//input[@name='login']"));
        highlightElement(driver, loginButton);
        loginButton.click();

        WebElement errorLabel = driver.findElement(By.xpath("//li[contains(text(),': Invalid username.')]"));
        highlightElement(driver, errorLabel);
        assertTrue(errorLabel.getText().contains("ERROR: Invalid username. "));
    }

    @Test
    public void loginTest_8() throws InterruptedException {

        driver.navigate().to(url);
        WebElement remembermeLabel = driver.findElement(By.xpath("//span[contains(text(),'Remember me')]"));
        highlightElement(driver, remembermeLabel);

        WebElement rememberCheckbox = driver.findElement(By.cssSelector("#rememberme"));
        rememberCheckbox.click();

        highlightElement(driver, remembermeLabel);
        assertTrue(rememberCheckbox.isSelected());
        Thread.sleep(3000);

        rememberCheckbox.click();
        highlightElement(driver, remembermeLabel);
        assertFalse(rememberCheckbox.isSelected());
    }

    @Test
    public void loginTest_9() {

        driver.navigate().to(url);
        WebElement usernameTextBox = driver.findElement(By.id("username"));
        highlightElement(driver, usernameTextBox);
        usernameTextBox.sendKeys("for_youlia");

        WebElement passwordTextBox = driver.findElement(By.cssSelector("#password"));
        highlightElement(driver, passwordTextBox);
        passwordTextBox.sendKeys("Password_Test@");

        WebElement loginButton = driver.findElement(By.xpath("//input[@name='login']"));
        highlightElement(driver, loginButton);
        loginButton.click();

        WebElement myAccountLabel = driver.findElement(By.cssSelector(".entry-title"));
        highlightElement(driver, myAccountLabel);
        assertEquals("MY ACCOUNT", myAccountLabel.getText());
    }

    @Test
    public void loginTest_10() {

        driver.navigate().to(url);
        WebElement lostPasswordLink = driver.findElement(By.linkText("Lost your password?"));
        highlightElement(
                driver, lostPasswordLink);
        lostPasswordLink.click();
        WebElement lostPasswordMessage = driver.findElement(By.xpath("//p[contains(text(),'Lost your password?')]"));
        highlightElement(driver, lostPasswordMessage);
        assertTrue(lostPasswordMessage.getText().contains("Lost your password? Please enter your username or email address."));
        WebDriverWait wait = new WebDriverWait(driver, 2);

        // List<WebElement> lostPassword = driver.findElements(By.linkText("Lost your password?"));
        // assertEquals(0, lostPassword.size());

        boolean notPresent = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText("Lost your password?")));
        assertTrue(notPresent);
    }

    //add testLoginElementsDisplayed()
    //add tests for valid-invalid username and password combinations
    //usernameField/ passwordField
    //add testValidLoginEmail()


}
