package shop;

import shop.pages.LoginPageFactory;
import utils.BaseTestClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import shop.model.User;

import static org.junit.Assert.assertEquals;

public class LoginPageFactoryTests extends BaseTestClass {

    String url = "https://testare-automata.practica.tech/shop/?page_id=560";

    @Test
    public void testLoginUsingPageFactory() {

        driver.navigate().to(url);
        LoginPageFactory loginPage = PageFactory.initElements(driver, LoginPageFactory.class);
        loginPage.fillUsername("for_youlia@yahoo.com");
        loginPage.fillPassword("Password_Test@");
        loginPage.clickLogin();

        WebElement myAccountLabel = driver.findElement(By.cssSelector(".entry-title"));
        assertEquals("MY ACCOUNT", myAccountLabel.getText());

    }

    @Test
    public void testLogin_1() {

        driver.navigate().to(url);
        LoginPageFactory loginPage = PageFactory.initElements(driver, LoginPageFactory.class);

        User user = new User.Builder().withEmail("for_youlia@yahoo.com").withPassword("Password_Test@").build();
        loginPage.loginWithEmailUser( user);

       // loginPage.loginWithUsernameAndPassword("for_youlia@yahoo.com", "Password_Test@");

        WebElement myAccountLabel = driver.findElement(By.cssSelector(".entry-title"));
        assertEquals("MY ACCOUNT", myAccountLabel.getText());

    }
}
