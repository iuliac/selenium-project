package shop;

import org.junit.Test;
import shop.pages.CollectionProductsList;
import shop.pages.CollectionProduct;
import shop.pages.HomePageCollection;
import shop.pages.HomePageContent;
import utils.BaseTestClass;

import static org.junit.Assert.assertEquals;

public class HomePageTestsDemo extends BaseTestClass {

    @Test
    public void checkMenCollectionTitle() {
        String expectedTitle = "Women Collection";
        HomePageContent homePageContent = new HomePageContent(driver);
        HomePageCollection homePageCollection = homePageContent.getHomePageCollectionByIndex(2);

        String actualTitle = homePageCollection.getCollectionTitle();
        assertEquals(expectedTitle, actualTitle);
    }

    @Test
    public void checkProductListSize() {
        HomePageContent homePageContent = new HomePageContent(driver);
        HomePageCollection collection = homePageContent.getHomePageCollectionByIndex(1);
        CollectionProductsList productsList = collection.getcollectionProductList(1);

        int numberOfProducts = productsList.getNumberOfProducts();
        assertEquals(5, numberOfProducts);
    }

    @Test
    public void verifyProductDetails() {
        HomePageContent homePageContent = new HomePageContent(driver);
        HomePageCollection collection = homePageContent.getHomePageCollectionByIndex(1);
        CollectionProductsList productList = collection.getcollectionProductList(1);
        CollectionProduct product = productList.getProductFromList("Classic Watch");

        assertEquals("Classic Watch", product.getTitle());
        assertEquals("Price: 70,00 lei", product.getPrice());
    }
}
