package files;

import org.junit.Test;

import java.io.File;
import java.nio.file.Files;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class FilesTests {

    @Test
    public void testFileNameExtractionFromPath(){
        File winFile= new File("C:\\beta\\gamma.txt");
        assertEquals("gamma.txt", winFile.getName());

        File unixFile = new File("c/beta/gamma.txt");
        assertEquals("gamma.txt", unixFile.getName());
        assertFalse(unixFile.exists());

    }
}
