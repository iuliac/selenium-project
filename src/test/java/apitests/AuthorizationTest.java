package apitests;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;

public class AuthorizationTest {

    @Test
    public void testAuthorization() {
        given()
                .when()
                .get("https://api.trello.com/1/boards/sbLBUIMP")
                .then().statusCode(HttpStatus.SC_UNAUTHORIZED);
        //then().statusCode(401);
    }

    @Test
    public void testAuthorizationSuccessful() {
        given().auth().oauth("d5b5f05e8a4b93e538a4f1273bb50a44",
                "1e90a405918a284f74279a47dc7e487711a10d50f44de97788200c59b15997c3",
                "e9dcfa091019f6628624d52f20de935ac03add4402eef97e2920d0b65d18e555", "")
                .when()
                .get("https://api.trello.com/1/boards/sbLBUIMP")
                .then().statusCode(200);
    }

    @Test
    public void testAuthorizationUnsuccessful_1() {
        given().auth().oauth("d5b5f05e8a4b93e538a4f1273bb50a444",
                "1e90a405918a284f74279a47dc7e487711a10d50f44de97788200c59b15997c3",
                "e9dcfa091019f6628624d52f20de935ac03add4402eef97e2920d0b65d18e555", "")
                .when()
                .get("https://api.trello.com/1/boards/sbLBUIMP")
                .then().statusCode(401);
    }

    @Test
    public void testBoardName() {
        Response response = given().auth().oauth("d5b5f05e8a4b93e538a4f1273bb50a44",
                "1e90a405918a284f74279a47dc7e487711a10d50f44de97788200c59b15997c3",
                "e9dcfa091019f6628624d52f20de935ac03add4402eef97e2920d0b65d18e555", "")
                .when()
                .get("https://api.trello.com/1/boards/sbLBUIMP")
                .then()
                .statusCode(200)
                .body("name", equalTo("my board"))
                //.and().body("")
                .extract().response();

       // response.prettyPrint();
        String json = response.asString();
        JsonPath jsonpath = new JsonPath(json);
        String name = jsonpath.getString("name");
        System.out.println(name);
        assertEquals("my board", name );
    }

}
