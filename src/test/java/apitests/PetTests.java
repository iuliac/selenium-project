package apitests;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Test;
import petstore.Pet;

import javax.xml.transform.Result;
import java.sql.ResultSet;
import java.util.HashMap;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

public class PetTests {
    String petUrl = "https://petstore.swagger.io/v2/pet/";

    @Test
    public void testCreatePet_1() {
        Pet pet = new Pet("Wantsome");
        given()
                .contentType(ContentType.JSON)
                .body(pet)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .body("name", equalTo(pet.getName()))
                .extract().response().prettyPrint();
        //id: 9216678377732767704
    }

    @Test
    public void testCreatePet_2() {
        Pet pet = new Pet("Wantsome1");
        given()
                .contentType(ContentType.JSON)
                .body("{\"name\":\"Wantsome1\"}")
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .body("name", equalTo(pet.getName()))
                .extract().response().prettyPrint();
    }

    @Test
    public void testCreatePet_3() {
        HashMap<String, Object> objectAsMap = new HashMap<>();
        objectAsMap.put("name", "NewPet");
        objectAsMap.put("status", "alive");
        given()
                .contentType(ContentType.JSON)
                .body(objectAsMap)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .body("name", equalTo(objectAsMap.get("name")))
                .extract().response().prettyPrint();

        //id: 9216678377732767711
    }

    @Test
    public void testCreatePet_4() {
        HashMap<String, String> petAsMap = new HashMap<>();
        petAsMap.put("name", "Kitten");
        petAsMap.put("status", "vaccinated");
        given()
                .contentType((ContentType.JSON))
                .body(petAsMap)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .body("name", equalTo(petAsMap.get("name")))
                .extract().response().prettyPrint();
        //id: 9216678377732767716
    }

    @Test
    public void testDeserialization() {
        Pet retrievedpet = given().pathParam("petId", 12345)
                .get("https://petstore.swagger.io/v2/pet/{petId}").as(Pet.class);

        assertThat(retrievedpet.getName(), is(equalTo("Api doggie")));

    }

    @Test
    public void testPetDeserialization() {
        Pet retrievedPet = given()
                .get("https://petstore.swagger.io/v2/pet/9216678377732767711").as(Pet.class);
        assertThat(retrievedPet.getName(), is(equalTo("NewPet")));
    }

    @Test
    public void testPetCreation_ex() {

        Pet pet = new Pet(1550, "My pet");
        given()
                .contentType(ContentType.JSON)
                .body(pet)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(HttpStatus.SC_OK);

        assertEquals("My pet", pet.getName());
        assertEquals(1550, pet.getId());
    }

    @Test
    public void testPetUpdate_ex() {
        Pet pet = given().get("https://petstore.swagger.io/v2/pet/1550").as(Pet.class);
        pet.setName("your pet");
        given()
                .contentType(ContentType.JSON)
                .body(pet)
                .when()
                .put("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200);

        assertEquals("your pet", pet.getName());

    }

    @Test
    public void testPetUpdate_ex2() {
        Pet pet = given().pathParam("petId", 1550).get(petUrl + "{petId}").as(Pet.class);
        pet.setStatus("unavailable");
        given()
                .contentType(ContentType.JSON)
                .body(pet)
                .when()
                .put(petUrl)
                .then().statusCode(200);

        assertEquals("unavailable", pet.getStatus());

        Response response = get(petUrl + pet.getId());
        response.body().prettyPrint();
    }

    @Test
    public void testPetDelete_ex() {
        //create a new pet
        Pet cat = new Pet(1551, "Tom");
        given()
                .contentType(ContentType.JSON)
                .body(cat)
                .when()
                .post(petUrl)
                .then()
                .statusCode(200);

        Response response = get(petUrl + cat.getId());
        response.prettyPrint();

        //delete the pet
        given()
                .contentType(ContentType.JSON)
                .when()
                .delete(petUrl + cat.getId())
                .then()
                .statusCode(200);
        response = get(petUrl + cat.getId());
        System.out.println("After cat deletion: " + response.getStatusLine());

    }

}
