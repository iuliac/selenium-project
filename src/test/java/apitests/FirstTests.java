package apitests;

import org.junit.Test;

import java.util.List;

import static io.restassured.RestAssured.*;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.*;

public class FirstTests {

    private String expectedJson = "{\"id\":1550,\"category\":{\"id\":0,\"name\":\"string\"},\"name\":\"lola\",\"photoUrls\":[\"string\"],\"tags\":[{\"id\":0,\"name\":\"string\"}],\"status\":\"available\"}";

    @Test
    public void testStatusCode() {
        get("https://petstore.swagger.io/v2/pet/1550")
                .then().assertThat().statusCode(200);
    }

    @Test
    public void testStatusForInvalidPet() {
        get("https://petstore.swagger.io/v2/pet/888")
                .then().assertThat().statusCode(404);
    }

    @Test
    public void testResponseHeader() {
        get("https://petstore.swagger.io/v2/pet/1550")
                .then().assertThat().header("Content-Type", equalTo("application/json"));
    }

    @Test
    public void testFullBodyResponse() {
        get("https://petstore.swagger.io/v2/pet/1550")
                .then().assertThat().body(equalTo(expectedJson));
    }

    @Test
    public void testBodyPathNavigation() {
        get("https://petstore.swagger.io/v2/pet/12345")
                .then()
                .assertThat()
                .body("name", equalTo("Api doggie"))
                .and()
                .body("category.name", equalTo("string"));
    }

    @Test
    public void testStatusPet() {
        get("https://petstore.swagger.io/v2/pet/12345")
                .then()
                .assertThat()
                .body("status", equalTo("available"));
    }

    @Test
    public void testJsonPathAsString() {
        String animal = get("https://petstore.swagger.io/v2/pet/15515").asString();
        //  System.out.println(animal);
        List<String> cats = from(animal).getList("tags.findAll{it.id<51}.name");

        System.out.println(cats.toString());
    }


}
