package apitests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import static io.restassured.path.json.JsonPath.from;

import static io.restassured.RestAssured.*;
import static junit.framework.TestCase.assertTrue;
import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;

public class SecondTests {

    @BeforeClass
    public static void setUp(){
        authentication = basic("user", "pass");
    }

    @Test
    public void testNameValidation() {
        get("https://petstore.swagger.io/v2/pet/15515")
                .then()
                .assertThat()
                .body("name", equalTo("animal"));
    }

    @Test
    public void testTagsValidation() {
        get("https://petstore.swagger.io/v2/pet/15515")
                .then()
                .assertThat()
                .body("tags.name", hasItem("dog"));

    }

    @Test
    public void testBodyPathAndFilter() {
        get("https://petstore.swagger.io/v2/pet/12345")
                .then().body("tags.findAll{it.id>0}.name", hasItem("good doggy"));
    }

    @Test
    public void testGivenWhenThen() {
        given().contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/12345")
                .then().statusCode(200)
                .body("category.name", equalTo("string"))
                .and().contentType(ContentType.JSON)
                .and().body(containsString("id"));
    }

    @Test
    public void testStatusCode2() {
        given().contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/12345")
                .then().statusCode(HttpStatus.SC_OK)
                .body("category.name", equalTo("string"))
                .and().contentType(ContentType.JSON)
                .and().body(containsString("id"));
    }

    @Test
    public void exercise1() {
        given().contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/123455")
                .then().statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void exercise2() {
        given().contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/12345")
                .then().statusCode(200)
                .body("category.price", equalTo("15.5"));

    }

    @Test
    public void exercise3() {
        given().contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/12345")
                .then().statusCode(200)
                .body("category.id", equalTo("4"));
        //  .and().body(containsString("name"));
    }

    @Test
    public void exercise4() {
        given().contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/12345")
                .then().statusCode(200)
                .and().body(containsString("name"));
    }

    @Test
    public void testResponse() {
        Response response = get("https://petstore.swagger.io/v2/pet/12345");
        response.prettyPrint();
    }

    @Test
    public void restJsonPath() {
        Response response = get("https://petstore.swagger.io/v2/pet/12345");
        String json = response.asString();

        JsonPath jsonPath = new JsonPath(json);
        String actualCategory = jsonPath.getString("category.name");
        assertEquals("Wantsome Pet", actualCategory);
    }

    @Test
    public void restXmlPath() {
        Response response = given().header("Accept", "application/xml")
                .get("https://petstore.swagger.io/v2/pet/12345");
        response.prettyPrint();
        String xml = response.asString();

        XmlPath xmlPath = new XmlPath(xml);
        xmlPath.setRoot("Pet");
        String actualCategory = xmlPath.getString("category.name");
        System.out.println("Actual category= " + actualCategory);
        assertEquals("Wantsome Pet", actualCategory);
    }

    @Test
    public void exerciseJsonPath() {
        Response response = get("https://petstore.swagger.io/v2/pet/12345");
        //  response.prettyPrint();
        String json = response.asString();
        JsonPath jsonPath = new JsonPath(json);
        String secondPetName = jsonPath.getString("tags[1].name");
        int secondPetId = jsonPath.getInt("tags[1].id");
        System.out.println(secondPetName);
        System.out.println(secondPetId);

        assertEquals("good doggy", secondPetName);
    }

    @Test
    public void exerciseXmlPath() {
        Response response = given().header("Accept", "application/xml")
                .get("https://petstore.swagger.io/v2/pet/12345");
        response.prettyPrint();
        String xml = response.asString();

        XmlPath xmlPath = new XmlPath(xml);
        xmlPath.setRoot("Pet");
        String secondPetName = xmlPath.getString("tags.tag[1].name");
        System.out.println(secondPetName);

        assertEquals("good doggy", secondPetName);
    }

    @Test
    public void testResponseHeaders() {
        Response response = get("https://petstore.swagger.io/v2/pet/12345");
        Headers headers = response.getHeaders();
        assertEquals(7, headers.size());
        assertTrue(headers.hasHeaderWithName("Server)"));
    }

    @Test
    public void testGetHeaderWithDate() {
        Response response = get("https://petstore.swagger.io/v2/pet/12345");
        String date = response.getHeader("Date");
        String[] tokens = date.split(",");
        assertEquals("Sat", tokens[0]);

    }

    @Test
    public void testResponseStatusCode() {
        Response response = get("https://petstore.swagger.io/v2/pet/12345");
        int statusCode = response.getStatusCode();
        assertEquals(200, statusCode);
        assertThat(statusCode, equalTo(200));
    }

    @Test
    public void testPathParams_1() {
        given().when().get("https://petstore.swagger.io/v2/pet/{petId}", 12345).then().statusCode(200);
    }

    @Test
    public void testPathParams_2() {
        given().when().get("https://petstore.swagger.io/{apiVersion}/pet/{petId}", "v2", 12345).then().statusCode(200);
    }

    @Test
    public void testPathParams_3() {
        given()
                .pathParam("apiVersion", "v2")
                .pathParam("petId", 12345)
                .when().get("https://petstore.swagger.io/{apiVersion}/pet/{petId}")
                .then().statusCode(200);
    }

    @Test
    public void testPathParams_exercise() {
        Response response = given()
                .pathParam("petId", 1550)
                .when().get("https://petstore.swagger.io/v2/pet/{petId}")
                .then().statusCode(200)
                .extract().response();
    }

    @Test
    public void testResponseTime_1() {
        long responseTime = given()
                .pathParam("petId", 12345)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .time();
        System.out.println("Response came back in " + responseTime + " miliseconds");
        assertThat(responseTime,(lessThan(5000L)));
    }

    @Test
    public void testResponseTime_2() {
        Response response = given()
                .pathParam("petId", 12345)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .time(lessThan(5L), TimeUnit.SECONDS).extract().response();
      //response actions..
    }
}
