package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

public class BrowserFactory {


    public static WebDriver getDriver(String browserName) {
        DriversUtils.setWebdriverProperty(browserName);
        switch (browserName) {
            case "chrome":
                return new ChromeDriver();

            case "firefox":
                return new FirefoxDriver();
            case "ie":
                return new InternetExplorerDriver();
            case "safari":
                return new SafariDriver();

        }
        throw new RuntimeException("This browser is not supported");

    }
}
