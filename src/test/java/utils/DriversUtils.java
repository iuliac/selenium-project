package utils;

import org.apache.commons.lang3.SystemUtils;

public class DriversUtils {
    public static void setWebdriverProperty(String browser) {
        switch (browser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", getDriversPath() + "chromedriver" + getFileExtemsion());
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", getDriversPath() + "geckodriver" + getFileExtemsion());
                break;

        }
    }

    private static String getProjectPath() {
        return System.getProperty("user.dir");

        //getProjectPath() + "\\src\\test\\resources\\driver\\windows\\chromedriver.exe"
    }

    private static String getDriversPath() {
        String driversPath;
        if (SystemUtils.IS_OS_WINDOWS) {
            driversPath = getProjectPath() + "\\src\\test\\resources\\driver\\windows\\";
        } else driversPath = "/src/test/resources/driver/mac/";
        return driversPath;
    }

    private static String getFileExtemsion() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return ".exe";
        }
        return "";
    }
}
