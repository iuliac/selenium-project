package utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static utils.DriversUtils.setWebdriverProperty;

public class BaseTestClass {

    public  WebDriver driver;
    String url ="https://testare-automata.practica.tech/shop/";

    @Before
    public void setup() {
       /* setWebdriverProperty("chrome");
        driver = new ChromeDriver();*/

       driver= BrowserFactory.getDriver("chrome");
       driver.get(url);
       driver.manage().window().maximize();

    }

    public static void highlightElement(WebDriver driver, WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: white; border: 2px solid red;');", element);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        js.executeScript("arguments[0].setAttribute('style','border: solid 3px white');", element);
    }

    public static void scrollToElement(WebDriver driver, WebElement element) throws InterruptedException {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
    }

    @After
    public void teardown(){
      //  driver.close();
    }

}
