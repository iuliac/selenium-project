package groups;

import data_driven.DataDrivenTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static junit.framework.TestCase.assertTrue;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        MediumTests.class,
        BasicTests.class,
        DataDrivenTests.class
})
public class SanityTests {



}
