import data_driven.DataDrivenTests;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class PropertiesTest {


    @Test
    public void loadPropertiesTest() throws IOException {
        File file= getFileFromResources("dev_props.properties");
        FileInputStream propsFile = new FileInputStream(file);
        Properties props= new Properties();
        props.load(propsFile);

        String browser= props.getProperty("browser");
        String addr= props.getProperty("server_addr");
        String port= props.getProperty("port");
        port=port==null? "80":port;

        assertEquals("chrome", browser);
        assertEquals("localhost", addr);
        assertEquals("80", port);
    }

    public static File getFileFromResources(String fileName) {
        ClassLoader classLoader = DataDrivenTests.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }
}
