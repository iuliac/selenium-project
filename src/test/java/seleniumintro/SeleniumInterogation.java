package seleniumintro;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Arrays;
import java.util.List;

import static utils.DriversUtils.setWebdriverProperty;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SeleniumInterogation {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebdriverProperty("chrome");
        driver = new ChromeDriver();
        driver.get("https://practica.wantsome.ro/utils");

    }

    @Test
    public void openChromeBrowser() throws InterruptedException {
        driver.get("https://practica.wantsome.ro/utils");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().to("https://www.emag.ro/");
        assertNotEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().back();
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
    }

    @Test
    public void pageSourceTest() {
        driver.get("https://practica.wantsome.ro/utils");
        System.out.println(driver.getPageSource());

    }

    @Test
    public void exercise1() {
        driver.get("https://practica.wantsome.ro/utils");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);

        String[] words = pageTitle.split(" ");
        System.out.println(Arrays.toString(words));

        int numberOfWordsWithMoreThan5Letters = 0;
        for (String word : words) {
            if (word.length() > 5) {
                numberOfWordsWithMoreThan5Letters++;
            }
        }

        assertEquals(5, numberOfWordsWithMoreThan5Letters);
    }

    @Test
    public void exercise2() {
        driver.get("https://practica.wantsome.ro/utils");
        String pageSource = driver.getPageSource();
        assertTrue(pageSource.contains("The roof is on fireeee"));

    }

    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following operations:
     * - print the page URL
     * - print the page title
     * - assert the fact that the page source contains 'Wantsome Iasi'
     */
    @Test
    public void firstBlogTest() {
        driver.get("https://practica.wantsome.ro/utils/");
        String pageURL = driver.getCurrentUrl();
        System.out.println(pageURL);
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        String pageSource = driver.getPageSource();
        assertTrue(pageSource.contains("Wantsome Iasi"));

    }

    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following actions:
     * - navigate to contact page and check page title
     * - navigate to login page and check that page source contains 'Remember Me'
     * - navigate to register page and check the current URL
     */
    @Test
    public void blogInterrogationTest() {
        driver.get("https://practica.wantsome.ro/utils/");
        driver.navigate().to("https://practica.wantsome.ro/utils/contact/");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        driver.navigate().to("https://practica.wantsome.ro/utils/login/");
        String pageSource = driver.getPageSource();
        assertTrue(pageSource.contains("Remember Me"));
        driver.navigate().to("https://practica.wantsome.ro/utils/register/");
        String currentUrl = driver.getCurrentUrl();
        assertEquals("https://practica.wantsome.ro/utils/register/", currentUrl);

    }

    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following operations:
     * - close the browser
     * - start again the browser and open the same URL
     * - navigate to Login page and check that you are on Login page (you have to think about a solution for this)
     * - navigate back and check you are again on homepage
     * - navigate to Register page
     * - go back
     * - navigate forward and check you are on Register page
     * - go back
     * - refresh the page and check you are on homepage
     * - close the browser
     */
    @Test
    public void navigationTest() throws InterruptedException {
        driver.get("https://practica.wantsome.ro/utils");
        driver.close();
        driver = new ChromeDriver();
        driver.get("https://practica.wantsome.ro/utils");
        driver.navigate().to("https://practica.wantsome.ro/utils/login/");
        String logintUrl = driver.getCurrentUrl();
        Thread.sleep(3000);
        assertEquals("https://practica.wantsome.ro/utils/login/", logintUrl);
        driver.navigate().back();
        String homeUrl = driver.getCurrentUrl();
        Thread.sleep(3000);
        assertEquals("https://practica.wantsome.ro/utils/", homeUrl);
        driver.navigate().to("https://practica.wantsome.ro/utils/register/");
        driver.navigate().back();
        driver.navigate().forward();
        String registerUrl = driver.getCurrentUrl();
        Thread.sleep(3000);
        assertEquals("https://practica.wantsome.ro/utils/register/", registerUrl);
        driver.navigate().back();
        driver.navigate().refresh();
        assertEquals("https://practica.wantsome.ro/utils/", homeUrl);

    }

    @Test
    public void findByCss() throws InterruptedException {
        //  driver.get("https://practica.wantsome.ro/blog");
        WebElement searchBox = driver.findElement(By.cssSelector(".search-field"));
        assertEquals(searchBox.getAttribute("title"), "Search for:");
        assertEquals("", searchBox.getText());

        searchBox.sendKeys("Alphabet");
        assertTrue(searchBox.getAttribute("value").contains("Alphabet"));


    }

    @Test
    public void findTitleOfAPost() {
        //  driver.get("https://practica.wantsome.ro/blog");
        List<WebElement> postTitle = driver.findElements(By.cssSelector("header>h2.entry-title"));
        for (WebElement element : postTitle) {
            assertTrue(element.getText().contains("Test"));
        }
    }

    @Test
    public void findByXPathTest() {
        WebElement searchBox = driver.findElement(By.cssSelector(".search-field"));
        searchBox.sendKeys("lorem");
        // searchBox.sendKeys(Keys.ENTER);
        WebElement searchButton = driver.findElement(By.className("search-submit"));
        searchButton.click();

        WebElement searchResultsLabel = driver.findElement(By.xpath("//h1[@class='archive-title']"));
        assertTrue(searchResultsLabel.getText().contains("Search Results"));

    }


    @AfterClass
    public static void teardown() {
        //driver.close();
    }
}
