package seleniumintro;

import utils.BaseTestClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class RegularTestExample extends BaseTestClass {

    String url = "http://testare-automata.practica.tech/shop";

    @Before
    public void before() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }


    // As a User I can click on My Account so that I reach the Login form
    // - navigation
    // - login page elements (initial status - e.g. fields are empty, elements are displayed/not displayed, etc.)
    //
    @Test
    public void loginPageNavigation() {
        driver.navigate().to(url);
        driver.findElement(By.xpath("//div[contains(@class, 'user-wrapper')]/a[@class = 'user-icon']")).click();
        assertEquals("MY ACCOUNT", driver.findElement(By.cssSelector(".page-header .tg-container h2")).getText());
    }

    // As a User I can enter my username and password in the Login form so that I can login into the application
    // - successful case (valid username + password)
    // - successful case (valid email + password)
    // - other cases (let's be creative :) )

}
