package seleniumintro;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import static utils.BaseTestClass.highlightElement;
import static utils.BaseTestClass.scrollToElement;
import static utils.DriversUtils.setWebdriverProperty;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MyFirstTests {

    private static WebDriver driver;
    private static WebDriverWait wait;
    // protected WebDriverWait wait;

    @BeforeClass
    public static void setup() {
        setWebdriverProperty("chrome");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 10);

    }

    @Test
    public void openChromeBrowser() throws InterruptedException {
        driver.get("https://practica.wantsome.ro/utils");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().to("https://www.emag.ro/");
        assertNotEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().back();
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
    }

    /*@Test
    public void openHtmUnitBrowser() throws InterruptedException {
        WebDriver driver = new HtmlUnitDriver();

        driver.get("https://practica.wantsome.ro/blog");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);

        Thread.sleep(3000);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        driver.close();
    }*/

    @Test
    public void selectTest() {
        driver.get("https://practica.wantsome.ro/shop/");
        driver.navigate().to("https://practica.wantsome.ro/shop/?product_cat=men-collection");
        WebElement sortDropdown = driver.findElement(By.cssSelector("select.orderby"));
        Select sortSelect = new Select(sortDropdown);
        sortSelect.selectByIndex(2);
        assertTrue(driver.getCurrentUrl().contains("orderby=rating"));
    }

    @Test
    public void verifyCheckoutCartButtonHover() throws InterruptedException {

        driver.get("https://practica.wantsome.ro/shop/");
        By specialXPath = getProductCartButton("Men's watch");

        WebElement addToCartButton = driver.findElement(specialXPath);

        // WebElement addToCartButton = driver
        //       .findElement(By.xpath("//a[contains(text(),'Men’s watch')]/parent::*/parent::*/div/a"));

        scrollToElement(driver, addToCartButton);
        highlightElement(driver, addToCartButton);
        Thread.sleep(2000);
        addToCartButton.click();


        WebElement cartTopElement = driver.findElement(By.xpath("//a[@class='wcmenucart-contents']"));
        scrollToElement(driver, cartTopElement);
        new Actions(driver)
                .moveToElement(driver.findElement(By.xpath("//a[@class='wcmenucart-contents']")))
                .build()
                .perform();

        Thread.sleep(2000);
        String xpathLocatorViewCart = "//div[@class='widget woocommerce widget_shopping_cart']//a[contains(text(),'View cart')]";
        WebElement viewCartElement = driver.findElement(By.xpath((xpathLocatorViewCart)));
        assertTrue(viewCartElement.isDisplayed());
    }

    @Test
    public void cookiesAreAwesome() {
        driver.get("https://practica.wantsome.ro/shop/");
        Set<Cookie> someCookies = driver.manage().getCookies();
        driver.manage().addCookie(new Cookie("wantsome cookies", "Yes, please"));
        assertEquals(2, driver.manage().getCookies().size());
        driver.manage().deleteAllCookies();
    }

    @Test
    public void testPageLoad() {
        // driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(4, TimeUnit.SECONDS);
        driver.navigate().to("https://singdeutsch.com/");
        By titleXpath = By.xpath("//h1[@class='site-title h1']");
        wait.until(ExpectedConditions.elementToBeClickable(titleXpath));
        assertEquals("SING DEUTSCH", driver.findElement(By.xpath("//h1[@class='site-title h1']")).getText());
    }

    @Test
    public void testAlerts() throws InterruptedException {
        driver.navigate().to("http://demo.guru99.com/test/delete_customer.php");
        driver
                .findElement(By.name("cusid"))
                .sendKeys("1234");
        driver
                .findElement(By.name("submit"))
                .click();
        Alert alert = driver.switchTo().alert();
        String alertMessage = alert.getText();
        System.out.println(alertMessage);
        Thread.sleep(3000);
        alert.dismiss();
    }

    @Test
    public void testIframe() {
        driver.get("http://demo.guru99.com/test/guru99home/");
        driver.manage().window().maximize();
        int size = driver.findElements(By.tagName("iframe")).size();
        System.out.println(size);
        driver.switchTo().frame("a077aa5e");
    }

    public By getProductCartButton(String product) {
        return By.xpath("//a[contains(text(),'Men’s watch')]/parent::*/parent::*/div/a");
    }
   /* public  void scrollToElement(WebElement element) throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
    }*/

    @AfterClass
    public static void teardown() {
        driver.close();
    }
}
